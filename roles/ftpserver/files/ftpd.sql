CREATE TABLE IF NOT EXISTS `ftpd`(
  `User` varchar(64) NOT NULL DEFAULT '',
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `Password` varchar(64) NOT NULL DEFAULT '',
  `Uid` varchar(4) NOT NULL DEFAULT '-1',
  `Gid` varchar(4) NOT NULL DEFAULT '-1',
  `Dir` varchar(256) NOT NULL DEFAULT '',
  `ULBandwidth` smallint(5) NOT NULL DEFAULT '0',
  `DLBandwidth` smallint(5) NOT NULL DEFAULT '0',
  `comment` tinytext NOT NULL,
  `ipaccess` varchar(15) NOT NULL DEFAULT '*',
  `QuotaSize` smallint(5) NOT NULL DEFAULT '0',
  `QuotaFiles` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`User`),
  UNIQUE KEY `User` (`User`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1