# .bashrc
# @author: Christophe Gossiaux
# @date: 17-06-2019
# @version: 2.0

# export
export HISTSIZE=50000
export HISTFILESIZE=50000
export HISTIGNORE="&:ls:[bf]g:exit:history:exit:clear"
export HISTCONTROL=ignoreboth
export EDITOR="vim"
export LC_ALL="en_US.UTF-8"


# shopt
shopt -s autocd
shopt -s cmdhist
shopt -s histappend
shopt -s histreedit
shopt -s checkwinsize

# aliases
alias ls='ls --color=auto'
alias ssh='ssh -A'
alias grep='grep --color=auto'
alias sauce='source ~/.bashrc'
alias vish='vim ~/.bashrc'
alias vim='vim "+sy on" "+set mouse=r" "+set paste"'
alias wget='wget -c'
alias rm='rm -i'
alias mv='mv -i'


myBorg (){
    case "$1" in
      list)
            . /usr/local/etc/borgbackup.conf ; sudo borg list $BACKUPUSERHOST:$REPOSITORY
            ;;
      info)
            . /usr/local/etc/borgbackup.conf ; sudo borg info $BACKUPUSERHOST:$REPOSITORY::$2
            ;;
      break-lock)
            . /usr/local/etc/borgbackup.conf ; sudo borg break-lock $BACKUPUSERHOST:$REPOSITORY
            ;;
      *)
            echo -e "launch file at: /usr/local/borg \nconfig file at: /usr/local/etc/borgbackup.conf"
            ;;
    esac
}

# Extract command
extract () {
     if [ -f $1 ] ; then
         case $1 in
             *.tar.bz2)   tar xjf $1        ;;
             *.tar.gz)    tar xzf $1     ;;
             *.bz2)       bunzip2 $1       ;;
             *.rar)       rar x $1     ;;
             *.gz)        gunzip $1     ;;
             *.tar)       tar xf $1        ;;
             *.tbz2)      tar xjf $1      ;;
             *.tgz)       tar xzf $1       ;;
             *.zip)       unzip $1     ;;
             *.Z)         uncompress $1  ;;
             *.7z)        7z x $1    ;;
             *)           echo "'$1' cannot be extracted via extract()" ;;
         esac
     else
         echo "'$1' is not a valid file"
     fi
}


# Transfer text file to link
transfer() {
  if [ $# -eq 0 ]; then echo "No arguments specified. Usage:\necho $0 /tmp/test.md";
  return 1;
  fi;
  curl 'https://files.cblue.be/index.php' -H 'Host: files.cblue.be'  -H 'Authorization: Basic Y2JsdWU6dXBsb2Fk'  -F "action=upload" -F "method=curl" -F "userfile=@$1";
}

netgrep() {
  sudo netstat -laptn | grep $1
}

# Create a Backup file with date
bak () {
  sudo cp $1 $1`date +%Y%m%d`.bak ;
}


export EDITOR='vim'
if [ -f ~/.bin/tmuxinator.bash ]; then
	source ~/.bin/tmuxinator.bash
fi

if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi



pwg() {
  LANG=C
  local l=$1
  [ “$l” == “” ] && l=$1
  tr -dc A-Za-z0-9_ < /dev/urandom | head -c ${l} | xargs
}

addsh () {
scp ~/.bashrc $1:~ && scp ~/.trueline.sh $1:~
}


declare -A TRUELINE_COLORS=(
[black]='36;39;46' #24272e
[cursor_grey]='40;44;52' #282c34
[default]='36;39;46' #24272e
[green]='152;195;121' #98c379
[grey]='171;178;191' #abb2bf
[light_blue]='97;175;239' #61afef
[mono]='130;137;151' #828997
[orange]='209;154;102' #d19a66
[purple]='198;120;221' #c678dd
[red]='224;108;117' #e06c75
[special_grey]='59;64;72' #3b4048
[white]='208;208;208' #d0d0d0
[blu]='77;187;249'
[gree]='60;224;142'
[blue_git]='41;41;97'
[orange_git]='252;163;38'
)

declare -a TRUELINE_SEGMENTS=(
'user,black,blu'
'venv,black,purple'
'git,orange_git,blue_git'
'working_dir,black,gree'
'read_only,black,orange'
'exit_status,black,red'
)

declare -A TRUELINE_SYMBOLS=(
[git_ahead]=''
[git_behind]=''
[git_bitbucket]=''
[git_branch]=''
[git_github]=''
[git_gitlab]=''
[git_modified]='✚'
[newline]='  '
[ps2]='...'
[read_only]=''
[segment_separator]=''
[ssh]=''
[venv]=''
[vimode_cmd]='N'
[vimode_ins]='I'
[working_dir_folder]=' '
[working_dir_home]=' '
#[working_dir_separator]='  '
[working_dir_separator]='  '
)

TRUELINE_GIT_SHOW_STATUS_NUMBERS=false
TRUELINE_GIT_MODIFIED_COLOR='grey'
TRUELINE_WORKING_DIR_SPACE_BETWEEN_PATH_SEPARATOR=false


source ~/.trueline.sh
